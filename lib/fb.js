

handlePhotoResponse = function (response , callback) {
      console.log(response);
      for (var i = 0 ; i < response.data.length; i++) {
             var photo = response.data[i];
             console.log(photo);
             FB.api(photo.id+"/picture" , function(response) {
			    callback(response.data.url);
             });
      }
      if (response.paging.next)
      {
         FB.api(response.paging.next, function(response) {handlePhotoResponse(response, callback);});    
      }
}

loadPics = function(type, callback) {
FB.api("/me/photos?type="+type, function(response) {handlePhotoResponse(response, callback);} );
}


fbLogin = function(callback) {

 if (fbConnection == true) {
     loadPics("uploaded" , callback);
 } 
 else {
  FB.login(function(response) {
      if (response.status === 'connected') {
        loadPics("uploaded" , callback);
      } else if (response.status === 'not_authorized') {
        alert("Не получилось зайти на Facebook. Попробуйте перезагрузить страницу.");
      } else {
        alert("Не получилось зайти на Facebook. Попробуйте перезагрузить страницу.");
      }
    }, {scope: 'public_profile,email,user_photos,user_tagged_places'});
  }
}

var fbConnection = false;

function fbInit() {
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '247439372333051',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
          fbConnection = true;
       }
      });
    
  };
  
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));  
} 

function fbLoad(callback) {
  fbLogin(callback); 
}