// M2IAAS INC (c) 2016 
/*    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


var GameSettings = 0;
var report_error_url = "http://www.psytechlab.net/report_error";
var cards_source = [
{src: "http://cs2.livemaster.ru/storage/38/55/046dbf8950be5d9bea46d9ecc9gh--suveniry-podarki-metaforicheskie-assotsiativnye-karty.jpg" , rows : 3 , columns:3 , offset_x : 2 , offset_y : 0} ,
{src: "http://deti.domateplo.ru/pics/Metaforicheskie_associativnie_karti__SAGA__4_o.jpeg" , rows : 3 , columns:3 , offset_x : 10 , offset_y : 12}   ,
{src: "http://cs1.livemaster.ru/storage/6a/34/a36d6d579712b8a138209a6907jt--suveniry-podarki-skachat-metaforicheskie-karty.jpg" , rows : 2 , columns:4 , offset_x : 10 , offset_y : 10}   ,
{src: "http://projective-cards.ru/images/cms/data/karti_nemeckie/tandu/03.jpg" , rows : 3 , columns:3 , offset_x : 2 , offset_y : 0}   ,
{src: "http://shop.imaton.com/mod_files/catalog_1_images/img_img_catalog_1_images_439.jpg" , rows : 3 , columns:3 , offset_x : 10 , offset_y : 10}   ,
{src: "http://samopoznanie.ru/avatars/objects/3-129047_1_6.jpg?423c0e86eb202c5a0523c6860ce7c3c3" , rows : 3 , columns:3 , offset_x : 10 , offset_y : 10}   ,
{src: "http://shop.imaton.com/mod_files/catalog_1_images/img_img_catalog_1_images_437.jpg", rows : 3 , columns:3 , offset_x : 2 , offset_y : 0},
{src: "http://cs1.livemaster.ru/storage/d1/2d/3ce2f6cfdb2a3725085d0b355awc--suveniry-podarki-skachat-metaforicheskie-karty.jpg", rows : 2 , columns:4 , offset_x : 0 , offset_y : 0}
];

// aux functions
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function loadCSS(href) {
  var cssLink = $("<link>");
  $("head").append(cssLink); //IE hack: append before setting href
  cssLink.attr({
    rel:  "stylesheet",
    type: "text/css",
    href: href
  });
}

// tutorial on webpage, see https://alvaroveliz.github.io/aSimpleTour/
function startTour() {
// tour default 
  var tour = {
   buttons : { next : {text: 'Далее &rarr;'} , prev : {text:'&larr; Назад'}, start : {text:'Начать'}, end: {text:'Пропустить Обучение'} },
   data : [
    { element: '.game', position:"T" , 'tooltip' : 'Игра',  text: "<h2>Введение</h2>Перед началом игры нужно определить свой запрос, подумайте<p><li>В чем вы хотите разобраться?<li>Что изменить?<li>Что Вас волнует?<br></p><p>Придумали запрос? Советуем записать его на бумаге..." }, // maybe to focus on 'problem'/'inquiry' textbox
    { element: '.game_field', 'tooltip' : 'Игровое поле' , position:"T" , text: "<h2>Игровое поле</h2><p>Создайте личное игровое поле</p><p>В нашей игре, игрок сам строит свое игровое поле, по-которому он будет перемещаться. </p><p>Поле пока не заполнено. Нажмите Далее.</p>" },
    { element: '.draggable_box2', 'tooltip' : 'Карточки сфер' , position:"T" , text: "<h2>Карточки сфер</h2><p>Тут находятся все доступные карточки, обозначающие ту или иную сферу жизни.</p><p>Нажмите Далее.</p>" },
    { element: '.top_half', 'tooltip' : 'Игровое поле' , position:"T" , text: "<h2>Строим игровое поле</h2>Теперь построим личное игровое поле.</p><p>Переместите мышкой (или пальцем на тачскрине) карточки сфер на поле, так чтобы в одной клетке находилось ровно по одной карточке.</p><p>Руководствуйтесь вашим запросом и интуицией при выборе в какую ячейку положить карточку.</p><p>Нажмите Далее, когда будет готово, чтобы начать игру.</p>" },
    { element: '.draggable_player_at_center', 'tooltip' : 'Игрок' , position:"T" , text: "<h2>Фигурка игрока</h2><p>Эта фигурка обозначает ваше место на игровом поле.</p><p>Вы можете свободно перемещать эту фигурку по полю.</p>Нажмите Далее, чтобы определить  начальную точку игрока на поле.</p>" },
    { element: '.game', 'tooltip' : 'Переместите фигурку игрока' , position:"T" , text: "<h2>Начальная точка</h2><p>Бросьте кубик. Номер, выпавший на кубике - это ваша стартовая точка. Нажмите Далее.</p>" },
    { element: '.game_field', 'tooltip' : 'Игровое поле' , position:"T" , text: "<h2>Игра начинается</h2><p>Первый ход.</p>Посмотрите на карточку сферы, которая помещена в клетку вашей стартовая точка.</p><p>Подумайте, как текст карточки в клетке, где стоит ваша фигурка соотносится с вашим запросом?</p><p>Нажмите Далее</p>" },
    { element: '#makButton1', 'tooltip' : 'Нажмите на кнопку, чтобы получить ассоциативную карту' , position:"LB" , text: "<h2>Карты</h2><p>Теперь подключим Ваши ассоциации. Они помогут Вашим размышлениям.</p><p>Нажмите на кнопку для того, чтобы получить карту.</p><p>Нажмите Далее</p>" },
    { element: '.img_thumbnail', 'tooltip' : 'Ваша карта' , position:"B" , text: "<h2>Карта</h2><p>Нажмите на карту, чтобы ее увеличить.</p><p>Порассматривайте карту, какие у вас возникают ассоциации?</p>Подумайте как карта связана с вашим запросом?</p><p>Как она связана с той темой, что указана на карточке сферы в той клетке, где вы находитесь?</p><p>Не торопитесь.. Ассоциации и возникающие мысли и есть основная польза нашей игры.</p><p>Запишите результат размышления и нажмите Далее</p>" },
    { element: '.game', 'tooltip' : 'Игра' , position:"T" , text: "<h2>Правила</h2><p>Сделаем следующий ход игры.</p><p>Бросьте кубик. Переместите вашу фигурку на значение, которое выпало на кубике.</p> На новой клетке: <li>Прочитайте текст карточки в клетке <li>Подумайте как он связан с Вашим запросом <li>Вытяньте карту <li>Проанализируйте собственные ассоциации, как они связаны с запросом и текстом карточки<li>Запишите результат размышления.<li>Бросьте кубик для перехода на следующую клетку.</p><p>Вот собственно и все правила игры. Нажмите Далее.</p>" },
    { element: '.game', 'tooltip' : 'Игра' , position:"T" , text: "<h2>Окончание</h2><p>Игра оканчивается тогда, когда вы сделаете 10 ходов.</p><p>Удачной игры и хороших мыслей!</p>" }
  ],
  controlsPosition : 'TL',
  welcomeMessage: "<h2>Привет!</h2><p>Добро пожаловать в первую онлайн психологическую трансформационную игру.</p><p>Игра в простой игровой форме помогает разобраться в самом себе.<p>Игра длится 15-20 минут и не требует ничего кроме внимания и воображения.</p> <p>Правила очень простые.</p>" 
  }

 // TODO: Download as JSON
 $.aSimpleTour(tour);
}

function enableDrag() {
 // set drag
    $( ".draggable, .draggable-nonvalid" ).draggable();
    $( ".draggable_player" ).draggable();
    $( ".draggable_player_at_center" ).draggable();
    $( ".droppable, .droppable_visible" ).droppable({
      myobj: 0,
      tolerance: "pointer",
      accept: ".draggable",
      classes: {
        "ui-droppable-active": "ui-state-active",
        "ui-droppable-hover": "ui-state-hover"
      },
      drop: function( event, ui ) {
        this.myobj = ui.draggable;
        $( this )
          .addClass( "ui-state-highlight" )
      },
     activate: function( event, ui ) {
        if (this.myobj == ui.draggable) {
           this.myobj = 0;
           $( this )
               .removeClass( "ui-state-highlight" )
         }
      }
    });
}

function enableHintTooltips() {
  $(".field_hint").on({
  "click": function() { 
      var hints = $(this).data("hints");
      var hint = hints[getRandomInt(0,hints.length)];
      $(this).tooltip({ items: "#"+$(this).attr("id"), content: hint, my: "center bottom+80" });
      $(this).tooltip("open");
  },
  "mouseout": function() {    
     $(this).tooltip("disable");   
  }
  });
  
}

function endLoading() {
  $("#loader-wrapper").hide();
}

function finishInit() {
// enable drag and drop
     enableDrag();
     enableHintTooltips();
     $(".field_background").imagesLoaded( function() {
       endLoading();
     });
}


// applies game parms from config urls given
function applyGameParms(cfgurl, makurl) {
  if (cfgurl == "local") {
    finishInit();
    return;
  }

// Apply settings:
// Game config, words, etc:
   $.ajax({
    url: cfgurl,
    error: function (request, status, error) {
        alert("Cannot load" + cfgurl);
        alert(request.responseText);
    }
    }).done(function( data ) {
    GameSettings = JSON.parse(data);
    // set text
     $( ".draggable").each (function (index, value) { 
             if (GameSettings["spheres"].length > index)
                 value.textContent = GameSettings["spheres"][index]["text"];
             else 
                 value.style.display="none"; // delete all extra blocks
      });
     if (GameSettings["field"]) {
         for (var key in GameSettings["field"]) {
                  $('#'+key).html(GameSettings["field"][key]);
         }
     }
     if (GameSettings["custom_field"]) {
        $(".game_field_line").each(function(line_index, line_obj) {
              line_descriptor = GameSettings["custom_field"][line_index];
              $(line_obj).find(".game_field_place").each(function(place_index, place_obj){
                 place_descriptor = line_descriptor["places"][place_index];
                 if (place_descriptor["class"]) $(place_obj).addClass(place_descriptor ["class"]);
                 if (place_descriptor["id"]) $(place_obj).attr("id",place_descriptor["id"]);
                 if (place_descriptor["content"]) $(place_obj).html(place_descriptor["content"]);
                 if (place_descriptor["hints"]) { $(place_obj).data("hints" , place_descriptor["hints"]); $(place_obj).addClass("field_hint"); }
              });
        });
     }
     if (GameSettings["css"]) {
       loadCSS(GameSettings["css"]);
     }
     if (GameSettings["dice"]) {
      $("#set").val(GameSettings["dice"]);
     }
     if (GameSettings["makButton"]) {
      $("#makButton1").text(GameSettings["makButton"]);
     }
     if (GameSettings["video"]) {
      video_url = GameSettings["video"];
      $("#video_button").featherlight({iframe: video_url, iframeMaxWidth: '80%', iframeMaxWidth: '90%', iframeWidth: 1000, iframeHeight: 600});
     }
     if (GameSettings["tutorial"]) {
      tutorial = "<html>" + GameSettings["tutorial"] + "</html>";
      $("#tutorial_button").featherlight(tutorial , {type: "html" , variant:"tutorial_window"});
     }
    
    // $(window).on("load", function() {
      finishInit();
     //}); // wait till everything is loaded
     
     
    });
    if (makurl == "fb") {
      cards_source = [];
      fbInit();
    }
    else {
    // Cards
     $.ajax({
        url: makurl,
        error: function (request, status, error) {
          alert("Cannot load " + makurl);
          alert(request.responseText + " " + status);
        }
      }).done(function( data ) {
        cards = JSON.parse(data);
      // set text
         if (cards["mak"]) {
            cards_source = cards["mak"];
         }
      });
    }
}


  $( function() {
    
   var cfgurl = getParameterByName("cfg") || "cfg/default.json.txt";
   var makurl = getParameterByName("mak") || "cfg/mak.default.json.txt";
   if (!(typeof perspectivesBaseDir === 'undefined' || perspectivesBaseDir === null)) {
       cfgurl = perspectivesBaseDir + cfgurl;
       makurl = perspectivesBaseDir + makurl;
   }
   applyGameParms(cfgurl, makurl);

    
    // set buttons
   $("#makButton1").click(function(){
        
        if (cards_source.length == 0 && makurl == "fb") {
          // add photos from facebook instead of cards
           fbLoad(function (url) {
            cards_source.push({src: url, rows : 1 , columns : 1 , offset_x : 0 , offset_y : 0})
           } );     
        }
   
        var j = getRandomInt(0 , cards_source.length);
        var row = getRandomInt(0 , cards_source[j].rows);
        var col = getRandomInt(0 , cards_source[j].columns);
        lightbox_id="image"+j+"_"+row+"_"+col;
        img_src = cards_source[j].src;
        placeholder = $("#mak_placeholder").prepend('<div class="mak_img_href"><a href="'+img_src+'"><div class="img_thumbnail"><img id="'+lightbox_id+'" src="'+img_src+'"></img></div></a></div>');
        img_link = placeholder.find("a");
        img = img_link.find( "img" );
        console.log("Loading card " + img_src);
        $("#makButton1").prop('disabled', true);
        //card_loading = $("#mak_placeholder").prepend('<div class="mak_loading">Загрузка...</div>');
        // highlight thumbnail image
        img.load(function() {
                $(this).css('max-width', cards_source[j].columns*100+"%"); 
                x = Math.floor ( (col/cards_source[j].columns) * $(this).get(0).width) + cards_source[j].offset_x;
                y = Math.floor ( (row/cards_source[j].rows) * $(this).get(0).height);
                $(this).css('margin-left', "-"+x+"px");   $(this).css('margin-top', "-"+y+"px");
                pic_height =  Math.floor($(this).parent().width() * 1.4);
                $(this).parent().css('max-height', pic_height+'px');
                $("#makButton1").prop('disabled', false);
        });
        // add a lightbox via http://noelboss.github.io/featherlight/
        img_link.featherlight(img_src, {type:{image: true}, closeOnClick:   'anywhere' , variant: 'lightbox_full_img'  , afterOpen: function (event) { 
                   img = this.$content;
                   //img.css('max-width', cards_source[j].columns*100+"%"); 
                   x = Math.floor ( (col/cards_source[j].columns) * img.get(0).width) + cards_source[j].offset_x;
                   y = Math.floor ( (row/cards_source[j].rows) * img.get(0).height) + cards_source[j].offset_y;
                   h = Math.floor ( img.get(0).height / cards_source[j].rows ); 
                   w = Math.floor ( img.get(0).width / cards_source[j].columns );
                   img.css('max-height', "90%");
         }
        } );        
    });
  
  // SET UP ERROR REPORT
  $("#report_error_button").featherlight("#report_error" ,  {variant: 'lightbox_full_frame'  , afterOpen: function (event) {
        var link = report_error_url+"?log="+encodeURIComponent(log.history.join("  --  "));
        $(this.$content).find("#report_error_frame").attr('src' , link);
  }});

  //SET UP DICE
  try {
     dice_initialize(document.getElementById("canvas"));
  } catch (err) {
    console.log("Cannot init dice: " + err);
    if (err.stack) {
        console.log(err.stack);
    }
  }

  } );

