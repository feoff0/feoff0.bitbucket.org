// usage: log('inside coolFunc',this,arguments);
// http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
original_log = console.log;


window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push([].slice.apply(arguments).join("  "));
  if(original_log){
    var arr = Array.prototype.slice.call(arguments);
    for (index = 0; index < arguments.length; ++index) {
        original_log.call(console, arr[index]);
    }
  }
};


if (console) {
    
  console.log = function () {
   window.log.apply(window, arguments);
  }
  console.warn = function () {
   window.log.apply(window,arguments);
  }
  console.error = function () {
   window.log.apply(window,arguments);
  }
  

}

console.log("Browser: " + navigator.userAgent);
console.log("Screen: " + screen.width + "x"+screen.height);
console.log("URL: " + window.location.href);
